package com.mail.demo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClienteController {


    @GetMapping("/usuarios")
    public ResponseEntity<String> restMensaje() {
        String mensaje = "[{\"Nombre\": \"Carlos Sotomayor\",\"Correo\": \"carlossotom@gmail.com\"," +
                "\"direccion\": \"Estancia Norte\"}, " +
                "{\"Universidad\": \"UTPL\",\"Curso\": \"Procesos de Ingeniería de Software\"," +
                "\"Nombre\": \"Nelson Alexander Aranda Villa\",\"Correo\": \"alexarand32@gmail.com\"," +
                "\"direccion\": \"Las Pitas\"}]";

        return ResponseEntity.ok(mensaje);
    }
}


//@RestController
//public class restMensaje {
//
//    @GetMapping("/")
//    public ResponseEntity<String> restMensaje() {
//        String mensaje = "{\"Universidad\": \"UTPL\",\"Curso\": \"Procesos de Ingeniería de Software\"," +
//                "\"Alumno\": \"Nelson Alexander Aranda Villa\",\"Período\": \"Abr/Ago 2021\"," +
//                "\"Lenguaje de programación preferido\": \"Python\",\"Aspiración PostGraduación\": \"Seguir maestria en IA\"}";
//
//        return ResponseEntity.ok(mensaje);
//    }




